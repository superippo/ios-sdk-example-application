#import <UIKit/UIKit.h>
#import "SIBannerType.h"
#import "SIBannerDelegate.h"

/** superippo.com Banner View */
@interface SIBannerView : UIView<UIWebViewDelegate>

/** Type of banner selected */
#if TARGET_INTERFACE_BUILDER
@property(nonatomic) IBInspectable NSInteger bannerType;
#else
@property(nonatomic) SIBannerType bannerType;
#endif

/** The unique code of the banner */
@property(nonatomic,strong) IBInspectable NSString* bannerCode;

/** Set to NO to hide the close button */
@property(nonatomic) IBInspectable BOOL closeButton;

/** If setted this object will receive events notifications from the banner */
@property(nonatomic,strong) id<SIBannerDelegate> delegate;

/** Ask the server for a new banner */
- (void)requestBanner;

/** Set the banner view as visible */
- (void)showBanner;

/** Hide the banner if its currently on the screen */
- (void)hideBanner;

- (instancetype)initWithCoder:(NSCoder *)decoder;
- (instancetype)initWithFrame:(CGRect)frame;

@end
