#import <Foundation/Foundation.h>

/** Object used to track the installations of Applications on iOS */
@interface SITracker : NSObject

/** Function called to check if the application was just installed */
+ (void)installationTracking;


@end
