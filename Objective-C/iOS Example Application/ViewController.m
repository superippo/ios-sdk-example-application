#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

SIBannerRotator *rotator;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    rotator = [SIBannerRotator rotatorWithBannerView:self.banner];
    [rotator start];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
