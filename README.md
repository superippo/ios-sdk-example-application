# iOS SDK Example Application
This repository contains two fully function example applications with the latest version of the SDK included inside.
The projects are identical but written in two languages. The first is Objective-C the second in Swift.

Just open the project file in Xcode and press Run to try the application.
