#import <Foundation/Foundation.h>
#import "SIBannerView.h"

/** A simple banner rotator for SIBannerView with timings that 
 can be configured */
@interface SIBannerRotator : NSObject<SIBannerDelegate>

/** The banner view controlled by this rotator */
@property(strong,nonatomic) SIBannerView* bannerView;

/** Show the banner after this amount of seconds */
@property(nonatomic) int showAfter;

/** Keep the banner on the screen for this ammount of seconds then hides it */
@property(nonatomic) int hideAfter;

/** Make a request for a new banner after this ammount of seconds */
@property(nonatomic) int showAgainAfter;

/** Create a new instance of the rotator using a banenr view */
+ (instancetype)rotatorWithBannerView:(SIBannerView*)bannerView;

/** Start the banner rotator */
- (void)start;

@end
