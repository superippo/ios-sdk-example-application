import UIKit
import SuperIppoSDK

class ViewController: UIViewController {
    @IBOutlet weak var banner: SIBannerView!
    
    var rotator: SIBannerRotator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        rotator = SIBannerRotator.init(bannerView: banner)
        rotator.start()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

